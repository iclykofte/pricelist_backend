from setuptools import setup, find_packages

setup(
    name='PriceList Backend',
    version='0.1',
    url='https://bitbucket.org/iclykofte/pricelist_backend',
    license='MIT',
    author='Hayati Gonultas',
    author_email='hayati.gonultas@gmail.com',
    description='Simple price list management application written in Flask.',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
        'Flask',
        'Flask-SQLAlchemy',
        'itsdangerous',
        'Jinja2',
        'requests',
        'SQLAlchemy',
        'SQLAlchemy-Utils',
        'Werkzeug',
        'pymysql',
        'passlib',
        'Flask-HTTPAuth'
    ],
    entry_points='''
        [console_scripts]
        pricelist_server=pricelist_backend.__main__:main
    ''',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers, Users',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)