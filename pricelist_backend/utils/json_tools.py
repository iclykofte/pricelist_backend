import decimal
from pricelist_backend.model import Company, PriceList, Price
from sqlalchemy.ext.declarative import DeclarativeMeta
from flask import json


class AlchemyEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o.__class__, DeclarativeMeta):
            data = {}
            fields = o.__json__() if hasattr(o, '__json__') else dir(o)
            for field in [f for f in fields if f not in ['metadata', 'query', 'query_class']]:
                value = o.__getattribute__(field)
                try:
                    if isinstance(value, decimal.Decimal):
                        data[field] = str(value)
                    else:
                        json.dumps(value)
                        data[field] = value
                except TypeError:
                    print 'type error:', field
                    data[field] = None
            return data
        return json.JSONEncoder.default(self, o)



def CompanyDecoder(obj):
    if '__company__' in obj:
        company = Company(name=obj['name'])
        company.id = obj['id']
        company.price_lists = []
        for pl in obj['price_lists']:
            p = PriceListDecoder(pl)
            company.price_lists.append(p)

        return company

    return obj


def PriceListDecoder(obj):
    if '__pricelist__' in obj:
        pl = PriceList(id=obj['id'], filename=obj['filename'], products_row=obj['products_row'],
                         product_name_col=obj['product_name_col'], price_col=obj['price_col'], pageno=obj['pageno'])
        if 'prices' in obj:
            for price in obj['prices']:
                p = PriceDecoder(price)
                pl.prices.append(p)

        return pl

    return obj


def PriceDecoder(obj):
    if '__price__' in obj:
        price = Price(id=obj['id'], product_name=obj['product_name'], price=obj['price'])
        return price

    return obj