import sys
from pricelist_backend import app
from config import Config

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    app.run(host='0.0.0.0', port=Config.PORT, debug=True)


if __name__ == "__main__":
    main()
