# Import flask and template operators
from flask import Flask

# Import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('pricelist_backend.config.Config')

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)

from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

# Build the database:
# This will create the database file using SQLAlchemy
# db.create_all()
# !!! create admin user
#

from utils.json_tools import AlchemyEncoder
app.json_encoder = AlchemyEncoder

import controller
