import json

from flask import g
from flask import request
from utils.json_tools import CompanyDecoder, PriceListDecoder, AlchemyEncoder
from model import Company, Price, User
from pricelist_backend import app
from pricelist_backend import db
from flask import jsonify
from model import PriceList
from pricelist_backend import auth
from sqlalchemy import and_


@app.route('/companies')
@auth.login_required
def companies():
    return jsonify(db.session.query(Company).all())


@app.route('/company/add', methods=['POST'])
@auth.login_required
def add_company():
    c_client = json.loads(request.data)
    company = Company(c_client['name'])
    db.session.add(company)
    db.session.commit()
    return 'added'


@app.route('/company/delete/<int:id>', methods=['DELETE'])
@auth.login_required
def delete_company(id):
    c = db.session.query(Company).filter_by(id=id).first()
    db.session.delete(c)
    db.session.commit()
    return 'deleted'


@app.route('/company/update/<int:id>', methods=['PUT'])
@auth.login_required
def update_company(id):
    c = db.session.query(Company).filter_by(id=id).first()
    c_client = json.loads(request.data, object_hook=CompanyDecoder)
    c.update_model(c_client)
    db.session.commit()

    return 'updated'



@app.route('/pricelist/update/<int:id>', methods=['POST'])
@auth.login_required
def update_prices_of_pricelist(id):
    pricelist = db.session.query(PriceList).filter_by(id=id).first()
    pricelist_client = json.loads(request.data, object_hook=PriceListDecoder)
    pricelist.update_model(pricelist_client)
    db.session.commit()

    return 'updated'


@app.route('/products/<productname_querystr>')
@auth.login_required
def product_query(productname_querystr):
    filter_group = []
    #split search terms and make an "and" ed query
    for st in productname_querystr.split():
        s = '%%%s%%' % st
        filter_group.append(Price.product_name.ilike(s))

    q = db.session.query(Price, Company.name)\
        .join(PriceList).join(Company)\
        .filter(and_(*filter_group)).order_by(Price.price.desc()).all()

    return jsonify(q)


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

@app.route('/user/update/<username>', methods=['POST'])
@auth.login_required
def update_password(username):
    usr = db.session.query(User).filter_by(username=username).first()
    if not usr or g.user.username != username:
        return False

    c_user = json.loads(request.data)

    usr.hash_password(c_user['password'])
    db.session.add(usr)
    db.session.commit()

    return jsonify({'username': username}), 201


@app.route('/api/token', methods=['GET'])
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return jsonify({ 'token': token.decode('ascii') })