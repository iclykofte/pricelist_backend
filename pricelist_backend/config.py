import os


class Config(object):
    # Statement for enabling the development environment
    # SQLALCHEMY_ECHO=True

    DEVELOPMENT = False
    DEBUG = False

    BASE_DIR = os.path.abspath(os.path.dirname(__file__))

    DB_HOST = '172.17.0.2'
    DB_USER = 'price_user'
    DB_PASS = '123qwe'
    DB_NAME = 'price_db'

    # Define the database uri
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%s:%s@%s/%s?charset=utf8mb4' % \
                              (DB_USER, DB_PASS, DB_HOST, DB_NAME)

    THREADS_PER_PAGE = 2

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "secret"

    # Secret key for signing cookies
    SECRET_KEY = "secret"

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    #SQLALCHEMY_POOL_RECYCLE = 299
    #SQLALCHEMY_POOL_TIMEOUT = 20

    PORT=8080

class ProductionConfig(Config):
    DEVELOPMENT = False
    DEBUG = False


