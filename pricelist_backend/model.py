from pricelist_backend import db
from utils.common import multi_delete
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from pricelist_backend import app


class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(191), index = True, unique=True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=9*3600): # set expiration to 9 hours
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user

    @staticmethod
    def create_admin():
        u = User()
        u.username = 'admin'
        u.password_hash = pwd_context.encrypt('123qwe')
        db.session.add(u)
        db.session.commit()


class Company(db.Model):
    id = db.Column(db.Integer, db.Sequence('company_id_seq'), primary_key=True)
    name = db.Column(db.String(191), unique=True)
    price_lists = db.relationship('PriceList', backref=db.backref('company'), cascade='all, delete-orphan')
    __company__ = True


    def __init__(self, name):
        self.name = name
        self.id = None


    def __repr__(self):
        return "<Company(name='%s')>" % (self.name)

    def __json__(self):
        return ['__company__', 'id', 'name', 'price_lists']

    def update_model(self, company):
        self.name = company.name
        to_delete = []
        updated_price_lists = []
        for i, pl in enumerate(self.price_lists):
            found = False
            for j, p in enumerate(company.price_lists):
                if p.id == pl.id:
                    # update price list
                    pl.update_model(p)
                    found = True
                    updated_price_lists.append(j)
                    break

            # mark for deletion of price list
            if found == False:
                to_delete.append(i)

        # process deleted price list
        multi_delete(self.price_lists, *to_delete)

        # new added price lists
        for i, p in enumerate(company.price_lists):
            if i not in updated_price_lists:
                pl = PriceList()
                pl.update_model(p)
                self.price_lists.append(pl)


class PriceList(db.Model):
    id = db.Column(db.Integer, db.Sequence('excel_pricelist_id_seq'), primary_key=True)
    filename = db.Column(db.String(1024))
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))

    products_row = db.Column(db.Integer)
    product_name_col = db.Column(db.Integer)
    price_col = db.Column(db.Integer)

    pageno = db.Column(db.Integer)

    __pricelist__ = True

    prices = db.relationship('Price', backref=db.backref('pricelist'), cascade='all, delete-orphan')


    def __repr__(self):
        return '<PriceList %r>' % self.filename.encode('utf-8')

    def __json__(self):
        return ['__pricelist__', 'id', 'filename', 'products_row', 'product_name_col', 'price_col', 'pageno']

    def update_model(self, pricelist):
        self.filename = pricelist.filename
        self.products_row = pricelist.products_row
        self.product_name_col = pricelist.product_name_col
        self.price_col = pricelist.price_col
        self.pageno = pricelist.pageno

        to_delete = []
        updated_prices = []
        for i, price in enumerate(self.prices):
            found = False
            for j, p in enumerate(pricelist.prices):
                if p.id == price.id:
                    # update price list
                    price.update_model(p)
                    found = True
                    updated_prices.append(j)
                    break

            # mark for deletion of price list
            if found == False:
                to_delete.append(i)

        # process deleted price list
        multi_delete(self.prices, *to_delete)

        # new added price lists
        for i, p in enumerate(pricelist.prices):
            if i not in updated_prices:
                price = Price()
                price.update_model(p)
                self.prices.append(price)


class Price(db.Model):
    id = db.Column(db.Integer, db.Sequence('prices_id_seq'), primary_key=True)
    pricelist_id = db.Column(db.Integer, db.ForeignKey('price_list.id'))
    product_name = db.Column(db.String(1024))
    price = db.Column(db.NUMERIC(precision=10,scale=2))

    __price__ = True

    def __repr__(self):
        return '<Price %r>' % self.product_name.encode('utf-8')

    def __json__(self):
        return ['__price__', 'id', 'product_name', 'price']

    def update_model(self, price):
        self.product_name = price.product_name
        self.price = price.price