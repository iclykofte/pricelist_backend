# Price List Backend

Simple and yet powerful application to manage your price lists offered by suppliers, wholesalers, and dealers. 
It merges all prices, and provides a easy to use gui for user. If you have frequently changing price lists this app is for you.
This is the backend (server) application for the GUI application.

## Technology:
Flask, python-flask, sqlalchemy and HTTP auth for authentication.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Although you can develop on your physical environment, I highly recommend using virtual environment for development and docker for database.

Be sure to check docker file (under db folder) and follow the instructions to setup database.

Requirements are listed in requirements.txt and can be installed with pip command.

if you're installing software on your real os (not virtual environment) you might want to install pip and setuptools as well:

sudo apt-get install python-pip  
sudo apt-get install python-setuptools

I also recommend upgrading some critical python packages:

sudo pip install --upgrade setuptools  
sudo pip install --upgrade pip  

Now get dependencies:

Using pip:  
pip install -r requirements.txt

If you have difficulties with some of requirements try globally installing them (not on virtualenv), and enable global site packages on virtual environment.

I used mariadb for database. To create initial table(s) and admin user issue following commands in python shell (if you're in virtualenv use virtualenv's python shell). In project folder:  

get into python shell

\# python  
\# from pricelist_backend.model import *  

\# db.create_all()  
\# User.create_admin()

exit from python shell.

Once db scheme is created then start coding or using the server.

## Running the tests

Unfortunately there is no tests yet, this was a very quick application to write (less than a week). Yes it is not a good programming practice, but I'll add them as I find a break.

## Deployment

Set appropriate values for database settings and server port on the config.py and install software with setuptools:

sudo python setup.py install

executable can be found at (for ubuntu, other distros may put somewhere else): /usr/local/bin/pricelist_server

## Built With

python 2.7
and
see requirements.txt

## Contributing

Please feel free to contact me.

## Versioning

I use [git](https://git-scm.com/) for versioning.  

## Authors

* **Hayati Gonultas** - *Initial work* - [iclykofte](https://bitbucket.org/iclykofte/)

## License

This project is licensed under the MIT License.